let dates_ = {
  'SPARKS': '', // Sparks type
  'NINIBET': '',
  'MASSERIA': '',
  'LUKK': '',
  'SPQR': '',
  'STYFFZ': '',
  'BABYJUMPHOOK': '',
  'BRUNO': '',
  'DEDI22' : '',
  'MCRISTIANO': '',
  'LOGAN': '', // Logan type
  'INSIDESPORT': '',
  'SLAVAG' : '',
  'KOMSO': '' // Text
};

let start_ = moment().format();

// Company (set the text you want to detect)
let COMPANY = '@Bet9000';
if (TEST) COMPANY = '@MASBET'; // testing


const mutationObserver = new MutationObserver(mutations =>
  mutations.forEach(mutation => {
    const { type, target, attributeName } = mutation;

    // Go to channel
    if (type == 'childList' && target.className.includes('im_dialog_badge')) {
      const $a = target.parentElement.parentElement;
      if ($a.querySelector('.im_dialog_peer').textContent.trim()
        .split(' ')[1] == COMPANY)
        $a.dispatchEvent(new MouseEvent('mousedown'));

      return;
    }

    // Get dates
    if (type == 'childList' && target.className == 'im_history_message_wrap') {
      const $title = document.querySelector('.tg_head_peer_title');

      if (!$title || !$title.textContent.includes(COMPANY)) return;
      const channel = $title.textContent.split(' ')[0];

      if (!dates_[channel]) {
        dates_[channel] = moment().format();
        console.log(dates_);
      }

      return;
    }

    // Protections
    if (!['im_message_wrap clearfix', 'photo_modal_image'].includes(target.className) ||
    type == 'attribute' && attributeName != 'src') return;

    // We need all dates to proceed and 30 seconds start catching messages and images
    if (Object.values(dates_).some(v => !v) || moment().diff(start_, 'seconds') <= 30) {
      start_ = moment().format();
      return console.warn('Empty dates');
    }

    // Get messages
    if (type == 'childList' && target.className == 'im_message_wrap clearfix') {
      const $title = target.querySelector('.im_message_author');
      const $time = target.querySelector('.im_message_date_text');
      const $text = target.querySelector('.im_message_text:not([style])');
      const $thumb = target.querySelector('.im_message_photo_thumb');

      if (!$title || !$title.textContent.includes(COMPANY)) return;
      const channel = $title.textContent.split(' ')[0];

      if (!target.parentElement.parentElement.parentElement.lastElementChild
        .textContent.includes(target.textContent)) return console.warn('Rerendering');

      if (!$time) return;
      const time = moment($time.getAttribute('data-content'), 'h:mm:ss A')
        .format('H:mm:ss');

      if (!dates_[channel]) return;
      const dateTime =
        moment(moment().format('YYYY-MM-DD') + ' ' + time).format();

      if (dateTime >= dates_[channel]) {
        dates_[channel] = dateTime;

        console.log($title.textContent);
        console.log(dateTime);

        if ($text) {
          console.log($text.textContent);
          onMutation({title: $title.textContent, text: $text.textContent});
        } else if ($thumb)
          setTimeout(() => $thumb.click(), 500) // activates Get images
      }
    }

    // Get images
    if (type == 'attributes' && attributeName == 'src' &&
    target.className == 'photo_modal_image') {
      const $modal = document.querySelector('.modal.photo_modal_window')
      const $title = $modal.querySelector('.media_modal_author');
      const $img = $modal.querySelector('img.photo_modal_image');

      if (!$title || !$title.textContent.includes(COMPANY)) return;
      const channel = $title.textContent.split(' ')[0];
      if (!dates_[channel]) return;

      if ($img && $img.width > 300 && $img.src.includes('blob')) {
        console.log($title.textContent);
        console.log($img.src);
        console.log($img.width, $img.height);

        setTimeout(() => {
          onMutation({title: $title.textContent, photo: $img.src});
          if (document.querySelector('.photo_modal_window'))
            document.querySelector('.photo_modal_window').click();
        }, 1000);
      } else {
        setTimeout(() => {
          if (document.querySelector('.photo_modal_window'))
            document.querySelector('.photo_modal_window').click();
        }, 1000);
      }
    }
  }))


mutationObserver.observe(document.body, {
  childList: true,
  subtree: true,
  attributes: true
});
