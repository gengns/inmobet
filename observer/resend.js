// Canvas
const canvas_ = document.createElement('canvas');
const ctx_ = canvas_.getContext('2d');


// Generic text sender
function sendMessage(chat_id, text) {
  if (TEST) chat_id = C['TESTING']; // testing

  fetch(`${TELEGRAM_URL}/sendMessage`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      chat_id: chat_id,
      text: text.replace(new RegExp(COMPANY.slice(1), 'gi'), 'MASBET')
        .replace(/#/g, '\n#')
    })
  })
}


// Generic image sender
function sendPhoto(chat_id, blob) {
  if (TEST) chat_id = C['TESTING']; // testing

  const formData = new FormData();
  formData.append('chat_id', chat_id);
  formData.append('photo',  blob);

  const request = new XMLHttpRequest();
  request.open('POST', `${TELEGRAM_URL}/sendPhoto`);
  request.send(formData);
}


// Load, prepare and resend image
function loadIntoCanvas(chat_id, src) {
    let img = new Image();
    
    img.onload = function() {
      if (img.height <= 50) return sendMessage(chat_id, BE_READY);

      canvas_.width = img.width;
      canvas_.height = img.height;
      ctx_.drawImage(img, 0, 0);
      console.log(img.width, img.height, canvas_.width, canvas_.height)

      if ([C['LOGAN'], C['INSIDESPORT'], C['SLAVAG']].some(id => id == chat_id)) {
        ctx_.filter = 'contrast(400%)';
      } else {
        // Remove central logo
        ctx_.beginPath();
        ctx_.moveTo(0, 60);
        ctx_.lineTo(img.width, 60);
        ctx_.strokeStyle = 'white';
        ctx_.lineWidth = 12;
        ctx_.stroke();
        // Remove bottom right logo
        ctx_.beginPath();
        ctx_.moveTo(img.width/2 + 55, 92);
        ctx_.lineTo(img.width, 92);
        ctx_.lineWidth = 18;
        ctx_.stroke();
        // Add blue slice
        ctx_.beginPath();
        ctx_.moveTo(img.width/2 + 55, 92);
        ctx_.lineTo(img.width/2 + 60, 92);
        ctx_.strokeStyle = '#22809c';
        ctx_.stroke();
      }
      
      canvas_.toBlob(blob => sendPhoto(chat_id, blob));
    }
    img.src = src;
}


// Listen for any kind of message.
function onMutation({title, text, photo}) {
  if (title && title.includes(COMPANY)) {
    title = title.split(' ')[0];
    
    if (!Object.keys(C).includes(title)) return;
    
    if (text) sendMessage(C[title], text);
    else if (photo) loadIntoCanvas(C[title], photo);
  }
}
