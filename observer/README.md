# Observer
Chrome extension that listens for updates in Telegram Web App Channels and resends information (messages and/or images) after some changes to other channels acting as a real user.

Notes: 
* chat = channel
* Tokens, IPs, users and ids are examples

# Useful information to run this chrome extension

## Add extension to chrome
`chrome://extensions`

1. Click on developer mode
2. Load unpacked
3. Any change need reload the specific chrome extension


## Pack extension
`chrome://extensions`

1. Pack
2. Choose folder
3. Use file.csx


## Google Closure Compiler
```sh
google-closure-compiler --js=index.js --js_output_file=index2.js
google-closure-compiler --js=resend.js --js_output_file=resend2.js 
```

1. Duplicate observer folder
2. Compress and protect with Closure Compiler index and resend files
3. Ready for deploy to Windows
4. Unpack and add extension with developer mode on


## Get updates from Telegram bot
It has to be in a temporary public channel.

`https://api.telegram.org/bot<token>/getUpdates`

If you cannot see your chat id, remove your bot from the chat and add it back.