# Forwarder
Server script that listens for any kind of message in your Telegram channels and resends the information (messages and/or images) after some changes to other channels.

Notes: 
* chat = channel
* Tokens, IPs, users and ids are examples

# Useful information to run this script

## Get updates from Telegram bot
It has to be in a temporary public channel.

`https://api.telegram.org/bot<token>/getUpdates`

## Copy app to server
```sh
scp ./inmobetserver.zip albert@94.161.27.154:/home/albert/Apps
```

Update only the script
```sh
scp ./index.js albert@94.161.27.154:/home/albert/Apps/inmobetserver
```

## Check server
```sh
sudo nmap -sS -p22 94.161.27.154
```

## Access to server
```sh
ssh -p22 albert@94.161.27.154 # you don't need to write port if it's 22
```

## Open persisten terminal
```sh
screen
~/albert/Apps/inmobetserver
node index
screen -ls
screen -X -S 1643 quit # session id you want to kill
```

## Add service (when restart)
```sh
/etc/systemd/system/inmobetserver.service
```

## Control whether service loads on boot
```sh
systemctl enable inmobetserver.service
```
```sh
systemctl disable inmobetserver.service
```

## Manual start and stop
```sh
systemctl start inmobetserver.service
```
```sh
systemctl stop inmobetserver.service
```

## Restarting/reloading
```sh
systemctl daemon-reload # run if .service file has changed
systemctl restart
```

## Check process
```sh
htop
```

## Delete process
```sh
pkill -f index.js 
```
