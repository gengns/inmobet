// Telegram Chat Ids (set your own names and ids)
const C = {
  'TESTING': -1002213652191, // chat for testing
  'MARTA': -1001250272025,
  'NINIBET': -1001309039169,
  'MASSERIA': -1001650831065,
  'STYFFZ': -1001261280638,
  'LOGAN': -1001212471327,
  'WTOW': -1001212513634,
  'INSIDESPORT': -1001101477790,
  'BETBARBAROS': -1001213652991,
  'KOMSO': -1001230766718
};

// Company and user (set the text you want to detect)
const COMPANY = 'Bet9000'; 
const USER = 'albert';

// Message to your users
const be_ready = 'Open your account, Pick coming soon!';

// Telegram token (set your own token) 
const TOKEN = '522161053:AAH0jmAhf9cNSrFbX3PpKLpCxlw6zLtxcTE';
const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(TOKEN, {polling: true});

const { createCanvas, Image } = require('canvas')


// Generic text sender to your Telegram chats
function sendMessage(chat_id, txt) {
  chat_id = C['TESTING']; // testing
  bot.sendMessage(chat_id, txt);
}


// Generic image sender to your Telegram chats
function sendPhoto(chat_id, buf) {
  chat_id = C['TESTING']; // testing
  bot.sendPhoto(chat_id, buf);
}


// Load, prepare and resend image using HTML Canvas in the server side
// (do anything you want with your images)
function loadIntoCanvas(chat_id, photo) {
  bot.getFileLink(photo[2].file_id).then(link => {
    let img = new Image();
    
    img.onload = function() {
      const canvas = createCanvas(img.width, img.height);
      const ctx = canvas.getContext('2d');
      ctx.drawImage(img, 0, 0);

      if (img.height <= 50) return sendMessage(chat_id, be_ready);

      if ([C['MARTA'], C['NINIBET'], C['MASSERIA'], C['STYFFZ']].some(id => id == chat_id)) {
        // Remove central logo
        ctx.beginPath();
        ctx.moveTo(0, 60);
        ctx.lineTo(img.width, 60);
        ctx.strokeStyle = 'white';
        ctx.lineWidth = 12;
        ctx.stroke();
        // Remove bottom right logo
        ctx.beginPath();
        ctx.moveTo(img.width/2 + 55, 92);
        ctx.lineTo(img.width, 92);
        ctx.lineWidth = 18;
        ctx.stroke();
        // Add blue slice
        ctx.beginPath();
        ctx.moveTo(img.width/2 + 55, 92);
        ctx.lineTo(img.width/2 + 60, 92);
        ctx.strokeStyle = '#22809c';
        ctx.stroke();
      } else if([C['LOGAN'], C['WTOW']].some(id => id == chat_id)) {
        //ctx.filter = 'contrast(400%)'; // no yet
        ctx.globalCompositeOperation = 'overlay';
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, img.width, img.height);
      } else if([C['INSIDESPORT']].some(id => id == chat_id) && img.height <= 150) {
        ctx.globalCompositeOperation = 'overlay';
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, img.width, img.height);
      }

      ctx.globalCompositeOperation = 'multiply';
      ctx.font = 'bold 14px Helveltica Arial';
      ctx.fillStyle = "#f77b00";
      ctx.fillText('@INMOBET', img.width - 100, img.height - 3);
      
      canvas.toBuffer((err, buf) => {
        if (!err) sendPhoto(chat_id, buf);
      });
    }
    img.src = link;
  });
}

// Listen for any kind of message in your Telegram channels
// (decide what you want to do)
bot.on('message', msg => {
  const { chat, from, forward_from_chat, text, photo } = msg;
  let title;
  if (forward_from_chat)
    title = forward_from_chat.title;

  if (C['TESTING'] != chat.id) 
    return;

  // Say hi if someones say hi
  if (text == 'hi') {
    sendMessage(C['TESTING'], text);
    return;
  }

  // Send text or image
  if (title && title.includes(COMPANY) && from && from.first_name == USER) {
      title = title.split(' ')[0];
      
      if (!Object.keys(C).includes(title)) return;
      
      if (text) sendMessage(C[title], text);
      else if (photo) loadIntoCanvas(C[title], photo);
  }
});
