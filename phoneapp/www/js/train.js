/**
  @file train.js

  @author Génesis García Morilla <gengns@gmail.com>
  @date 2018-04

  @description Working with NeuralNetwork
*/
const train = (() => {
  const colorThief_ = new ColorThief();

  let network_ = new brain.NeuralNetwork({activation: "leaky-relu"});
  if (localStorage.network) network_.fromJSON(JSON.parse(localStorage.network));
  localStorage.trainingData = localStorage.trainingData || JSON.stringify([])

  function load() {
    axios.get('file:///android_asset/www/view/train.html').then(r => {
      $('#container').innerHTML = r.data;

      $("#backward").onclick = () => ready.load();

      
      $("#undo").onclick = () => {
        let data = JSON.parse(localStorage.trainingData);
        const imgData = data.pop();
        
        if (imgData) {
          alert(`Last image from ${Object.keys(imgData.output)} data is out.`);
          localStorage.trainingData = JSON.stringify(data);
        }
      }
      $("#delete").onclick = () => {
        if (localStorage.network)
          if (confirm('You are about to delete your training and training data! Are you sure?')) {
            network_ = new brain.NeuralNetwork({activation: "leaky-relu"});
            delete localStorage.network;
            localStorage.trainingData = JSON.stringify([]);
            alert('You have deleted your training and training data.');
          }
      };

      $$('input').forEach($input => $input.onchange = readFiles);

      $("#marta").onclick = () => saveTrainingData({ marta: 1 });
      $("#logan").onclick = () => saveTrainingData({ logan: 1 });
      $("#nothing").onclick = () => saveTrainingData({ marta: 0, logan: 0 });

      $("#train-now").onclick = () => trainData();
      $("#predict").onclick = () => predictImageType();
    });
  }


  function getData(img) {
    const palette = colorThief_.getPalette(img).join().split(',');

    return palette.reduce((acc, c, i) =>
      acc.concat(Math.round(c/2.55) / 100), []);
  }


  function saveTrainingData(score) { 
    const $img = $('img');
    if (!$img) return;
    let data = JSON.parse(localStorage.trainingData);

    data.push({
      input: getData($img),
      output: score
    });

    localStorage.trainingData = JSON.stringify(data);
    $img.remove();
  }


  function trainData() {
    const data = JSON.parse(localStorage.trainingData);
    
    if (!data.length) {
      alert('Please select a folder and choose type of images to start training' + 
      ' or delete your training and import trainingData.');
      return;
    }

    waiting_on();
    network_
      .trainAsync(data)
      .then(res => {
        waiting_off();
        if (res.error >= 0.4 || res.iterations >= 20000) {
          alert('Network failed to train!');
          return;
        }
        localStorage.network = JSON.stringify(network_.toJSON());        
        alert('Your network is trained!');
      })
      .catch(err => {
        waiting_off();
        alert('Error while training: ' + err);
      });
  }


  function predictImageType() {
    const $img = $('img');
    if (!$img) {
      alert('Select an image');
      return;
    }

    if (!network_.weights) {
      alert("You haven't train your data yet");
      return;
    }

    alert(JSON.stringify(network_.run(getData($img))));
  }


  function readFiles(e) {
    const files = e.target.files
    if (!files.length) return;

    [...files].forEach(file => {
      const image = new Image();
      image.src = URL.createObjectURL(file);
      $('main').appendChild(image);
      image.onclick = () => image.remove();
    });
  }


  function getPick(img) {
    if (!network_.weights) {
      alert("You haven't train your data yet");
      return;
    }

    return network_.run(getData(img));
  }


  function exportJSON() {
    if (!network_.weights) {
      alert("You haven't train your data yet");
      return;
    }

    const path = 'file:///storage/emulated/0/Download/';
    const file_name = 'Inmobet_' + toLocalISOString(new Date()).slice(0, -5)
      .replace(/:/g, '.')
      .replace('T', ' ') + '.json';
    const json = JSON.stringify(network_.toJSON());

    resolveLocalFileSystemURL(path, dirEntry => {
      create_file(dirEntry, file_name,
        new Blob([json], {type: "application/json"}));
    }, err => alert('Error: ' + err));
  }


  function importJSON(json) {
    if (!confirm('You are going to rewrite your training! Are you sure?'))
      return;

    network_.fromJSON(json);
    localStorage.network = JSON.stringify(json);
    alert('You got a new training!');
  }

  function getWeights() {
    return network_.weights;
  }


  // ---------------------------------------------------------------------------
  return {
    load: load,
    getPick: getPick,
    exportJSON: exportJSON,
    importJSON: importJSON,
    getWeights: getWeights
  }
})();
