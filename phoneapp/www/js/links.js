/**
  @file links.js

  @author Génesis García Morilla <gengns@gmail.com>
  @date 2018-05

  @description Channel names
*/
const links = (() => {
  function load() {
    axios.get('file:///android_asset/www/view/links.html').then(r => {
      $('#container').innerHTML = r.data;

      $('#backward').onclick = () => ready.load();

      if (localStorage.links) {
        const { title, channels } = JSON.parse(localStorage.links);

        $('#title').value = title;

        let html = '';
        channels.forEach(channel => {
          const { id, text, url } = channel;

          html +=
           `<div id="${id}">
             <h3>${id}</h3>
             <input type="text" value="${text}" placeholder="${id}">
             <input type="url" value="${url}" placeholder="https://t.me/joinchat/AAAAbFCM1tqKXDiID7K2dQ">
             <button class="mdc-button mdc-button--unelevated">Delete</button>
           </div>`;
        });

        $('main > div').insertAdjacentHTML('beforeend', html);

        [...$$('#links div[id]:not([class]) button')].forEach($button => {
          $button.onclick = () => remove($button.parentElement);
        });
      }

      $('#add').onclick = () => add();
      $('#save').onclick = () => save();
      $('#send').onclick = () => send();
    });
  }


  function remove($element) {
    if (confirm('You are going to delete this channel link. Are you sure?')) {
      $element.remove();
      alert('Done! Remember to save changes.');
    }
  }


  function add() {
    let id = prompt('Just define a lowercase name composed of a single word without numbers. For example: betbarbaros');

    if (!id) return;

    id = id.replace(/[^a-z]/g, '');

    if ($(`#${id}`)) {
      alert('It already exists, choose a different name.');
      return;
    }

    $('main > div').insertAdjacentHTML('beforeend',
       `<div id="${id}">
         <h3>${id}</h3>
         <input type="text" placeholder="${id}">
         <input type="url" placeholder="https://t.me/joinchat/AAAAbFCM1tqKXDiID7K2dQ">
         <button class="mdc-button mdc-button--unelevated">Delete</button>
       </div>`);

    $(`#${id} button`).onclick = () => remove($(`#${id}`));
  }


  function save() {
    const channels =
      [...$$('#links div[id]:not([class])')].reduce((arr, $channel) =>
          arr.concat({
            id: $channel.id,
            text: $channel.$('input[type="text"]').value,
            url: $channel.$('input[type="url"]').value
          })
      , []);

    if (!channels) {
      alert('There are not channels, please add some!');
      return false;
    }

    localStorage.links = JSON.stringify({
      title: $('#title').value,
      channels: channels
    });

    alert('Everything saved!');
    return true;
  }


  function send() {
    if (!confirm('You are going to save and send links. Are you sure?'))
      return;

    if (!save()) return;

    let { title, channels } = JSON.parse(localStorage.links);

    channels =
      channels.reduce((arr, channel) => {
        const { text, url } = channel;

        arr.push([{"text": text, "url": url}]);
        return arr;
      }, []);

    const chat_id = Number($('select').value);

    axios.post(`${TELEGRAM_URL}/sendMessage`, {
      chat_id: chat_id,
      text: title,
      reply_markup: {
        "inline_keyboard": channels
      }
    });

    alert('You have saved and sent links.');
  }


  // ---------------------------------------------------------------------------
  return {
    load: load
  }
})();
