/**
  @file gquery.js

  @author Génesis García Morilla <gengns@gmail.com>
  @date 2018-04

  @description Utilities
*/


// Short selectors like jQuery
function $(selector) {
  return document.querySelector(selector);
}


function $$(selector) {
  return document.querySelectorAll(selector);
}


Element.prototype.$ = function(selector) {
  return this.querySelector(selector);
}


Element.prototype.$$ = function(selector) {
  return this.querySelectorAll(selector);
}

/** Blocking and waiting */
function waiting_on() {
  $('#waiting').classList.remove("hide");
  document.body.classList.add("avoid-clicks");
}

function waiting_off() {
  $('#waiting').classList.add("hide");
  document.body.classList.remove("avoid-clicks");
}

/** Local ISO String Date */
function toLocalISOString(d) {
  const offset = d.getTimezoneOffset()
  return new Date(
    d.getFullYear(),
    d.getMonth(),
    d.getDate(),
    d.getHours(),
    d.getMinutes() - offset,
    d.getSeconds(),
    d.getMilliseconds()).toISOString()
}

/**
  Create local file

  @param dirEntry
  @param fileName 'name.txt'
  @param blob
*/
function create_file(dirEntry, fileName, blob) {
  dirEntry.getFile(fileName, {create: true, exclusive: false}, function(fileEntry) {
    write_file(fileEntry, blob);
  }, err => alert('Creating error: ' + err));
}

/**
  Write local file

  @param fileEntry
  @param blob
*/
function write_file(fileEntry, blob) {
  fileEntry.createWriter(function (fileWriter) {
    fileWriter.onwriteend = function() {
      alert("File exported to Download");
    };
    fileWriter.onerror = function (e) {
      alert("Writing error: " + e.toString());
    };
    fileWriter.write(blob);
  });
}

// If device is ready
document.addEventListener('deviceready', () => {
  const permissions = cordova.plugins.permissions;

  permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, status => {
    if (!status.hasPermission) {
      permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE, status => {
        if (!status.hasPermission) {
          alert('You need storage permission to use the app!');
          navigator.app.exitApp();
        }
      });
    }
  });
});
