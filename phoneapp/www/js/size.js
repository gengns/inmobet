/**
  @file size.js

  @author Génesis García Morilla <gengns@gmail.com>
  @date 2018-05

  @description Maximum image size
*/
const size = (() => {
  function load() {
    axios.get('file:///android_asset/www/view/size.html').then(r => {
      $('#container').innerHTML = r.data;

      $("#backward").onclick = () => ready.load();

      if (localStorage.size) {
        const { width, height } = JSON.parse(localStorage.size);

        $('#width').value = width;
        $('#height').value = height;
      }

      $("#save").onclick = () => save();
    });
  }


  function save() {
    if (![...$$('input')].every($input => {
      const value = Number($input.value);
      return value > 0 && value % 1 == 0;
    })) {
      alert('You have to fulfil everything with integers.');
      return;
    }

    localStorage.size = JSON.stringify({
      width: Number($('#width').value),
      height: Number($('#height').value)
    });

    alert('Everything saved!');
  }


  // ---------------------------------------------------------------------------
  return {
    load: load
  }
})();
