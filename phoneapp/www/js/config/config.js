/**
  @file config.js

  @author Génesis García Morilla <gengns@gmail.com>
  @date 2018-05

  @description Basic configuration
*/


//============================================================================
// GENERAL PARAMETERS
//============================================================================
// Telegram Chat Ids (set your own names and ids)
const CHAT_INFO = -1101129039169;
const CHAT_MARTA = -1011204723158;
const CHAT_BETBARBAROS = -1101352012820;
const CHAT_LOGAN = -1121201477790;


// Group id
const CHAT_INMOBET = -1102185292244;


// Local path
const PATH = 'file:///storage/emulated/0/Telegram/Telegram Images';


// Telegram Bot
const TOKEN = '522161053:AAH0jmAhf9cNSrFbX3PpKLpCxlw6zLtxcTE';
const TELEGRAM_URL = `https://api.telegram.org/bot${TOKEN}`;


/*
  See chats ids = https://api.telegram.org/bot522161053:AAH0jmAhf9cNSrFbX3PpKLpCxlw6zLtxcTE/getUpdates
  If you cannot see your chat id, remove your bot from the chat and add it back.
*/
