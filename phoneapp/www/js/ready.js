/**
  @file ready.js

  @copyright Génesis García Morilla <gengns@gmail.com>
  @author Génesis García Morilla <gengns@gmail.com>
  @date 2018-04

  @description Launch image detection
*/
const ready = (() => {
  let canvas_;
  let ctx_;
  localStorage.accuracy = localStorage.accuracy || JSON.stringify(0.6);


  let lastModifiedDate_ = Date.now();
  let timeout_; // period

  function load() {
    axios.get('file:///android_asset/www/view/ready.html').then(r => {
      $('#container').innerHTML = r.data;

      // MDC Toolbar
      const toolbar = mdc.toolbar.MDCToolbar.attachTo($('.mdc-toolbar'));
      toolbar.fixedAdjustElement = $('.mdc-toolbar-fixed-adjust');

      // MDC Drawer
      const MDCTemporaryDrawer = mdc.drawer.MDCTemporaryDrawer;
      const drawer = new MDCTemporaryDrawer($('.mdc-drawer'));
      $('#menu').onclick = () => drawer.open = true;

      // Interface elements
      canvas_ = $('canvas');
      ctx_ = canvas_.getContext('2d');

      // Connections
      $('#play').onclick = () => play();

      $('#stop').onclick = () => stop();

      $('#train').onclick = () => {
        stop();
        train.load();
      }

      $('#accuracy').onclick = () => {
        stop();
        let accuracy;
        do {
          accuracy = Number(prompt('Please enter a number from 0.0 to 1.0',
          localStorage.accuracy));
        } while(isNaN(accuracy) || accuracy < 0 || accuracy > 1);

        if (!isNaN(accuracy) && accuracy > 0 && accuracy < 1)
          localStorage.accuracy = accuracy;
      };

      $('#size').onclick = () => {
        stop();
        size.load();
      };

      $('#links').onclick = () => {
        stop();
        links.load();
      };

      $('#export').onclick = () => {
        stop();
        const path = 'file:///storage/emulated/0/Download/';
        const file_name = 'Inmobet_' + toLocalISOString(new Date()).slice(0, -5)
          .replace(/:/g, '.')
          .replace('T', ' ') + '.json';

        let json =
          Object.entries(localStorage).reduce((obj, p) => {
            if (p[0] != 'network')
              obj[p[0]] = JSON.parse(p[1]);
            return obj;
          }, {});

        if (!json) return;
        else json = JSON.stringify(json);

        const a = Object.keys(localStorage).filter(k => k != 'network');
        if (!confirm('You are going to export: ' + [a.slice(0, -1).join(', '),
           a.slice(-1)[0]].join(a.length < 2 ? '' : ' and ')))
           return;

        resolveLocalFileSystemURL(path, dirEntry => {
          create_file(dirEntry, file_name,
            new Blob([json], {type: "application/json"}));
        }, err => alert('Error: ' + err));
      };

      $('#import').onclick = () => {
        stop();
        const $input = $('#import').$('input');
        $input.click();
        $input.onchange = () => {
          const file = $input.files[0];

          if (file && file.type == "application/json") {
            const reader = new FileReader();
            reader.onload = event => {
              Object.entries(JSON.parse(event.target.result)).forEach(p => {
                localStorage[p[0]] = JSON.stringify(p[1]);
              });

              const a = Object.keys(JSON.parse(event.target.result));

              alert('You imported: ' +
                [a.slice(0, -1).join(', '),
                 a.slice(-1)[0]].join(a.length < 2 ? '' : ' and '));
            };
            reader.readAsText(file);
          } else alert('Invalid file!');
        }
      };

      const $beta = $('#beta');
      $beta.onclick = () => {
        stop();
        $beta.classList.toggle('on');

        if ($beta.classList.contains('on')) {
          $beta.$('span').textContent = 'ON';
          $('.mdc-toolbar__title').textContent = 'Test';
        } else {
          $beta.$('span').textContent = 'OFF';
          $('.mdc-toolbar__title').textContent = 'Ready';
        }
      };
    });
  }


  function setLine(name, type) {
    const html =
    `<li class="mdc-list-item">
      <span class="mdc-list-item__graphic" role="presentation">
        <i class="material-icons">${type}</i>
      </span>
      <span class="mdc-list-item__text">${name}
        <span class="mdc-list-item__secondary-text">${new Date().toLocaleString()}</span>
      </span>
      <a href="#" class="mdc-list-item__meta material-icons">info</a>
    </li>`;

    $('ol').insertAdjacentHTML('afterbegin', html);

    if (type == 'photo')
      $('#files').innerHTML =
        Number($('#files').innerHTML) + 1;
  }


  function sendPhoto(blob, chat_id) {
    if ($('#beta').classList.contains('on'))
      chat_id = CHAT_INMOBET;

    const formData = new FormData();
    formData.append('chat_id', chat_id);
    formData.append('photo',  blob);

    const request = new XMLHttpRequest();
    request.open('POST', `${TELEGRAM_URL}/sendPhoto`);
    request.send(formData);

    // Stop and relaunch after 1 minute
    stop();
    setTimeout(() => {
      stop(); // set lastModifiedDate_
      play();
    }, 60e3);
  }


  // Load, prepare and resend image
  function loadIntoCanvas(file) {
    const image = new Image();
    const accuracy = Number(localStorage.accuracy);
    let { width , height } = JSON.parse(localStorage.size);

    image.onload = function() {
      // Save date in global variable
      lastModifiedDate_ = file.lastModifiedDate;

      // Canvas size = image size
      canvas_.width = this.width;
      canvas_.height = this.height;

      // Get prediction
      const prediction = train.getPick(image);
      if (!prediction) return;

      if (this.height <= 50 || this.width > width || this.height > height)
        return;
      // Pick type Marta
      else if (prediction.marta > accuracy) {
        ctx_.drawImage(this, 0, 0);
        // Remove central logo
        ctx_.beginPath();
        ctx_.moveTo(0, 60);
        ctx_.lineTo(this.width, 60);
        ctx_.strokeStyle = 'white';
        ctx_.lineWidth = 12;
        ctx_.stroke();
        // Remove bottom right logo
        ctx_.beginPath();
        ctx_.moveTo(this.width/2 + 55, 92);
        ctx_.lineTo(this.width, 92);
        ctx_.lineWidth = 18;
        ctx_.stroke();
        // Add blue slice
        ctx_.beginPath();
        ctx_.moveTo(this.width/2 + 55, 92);
        ctx_.lineTo(this.width/2 + 60, 92);
        ctx_.strokeStyle = '#22809c';
        ctx_.stroke();
        // Send
        canvas_.toBlob(blob => sendPhoto(blob, CHAT_MARTA));
        setLine(file.name, 'photo');
      }
      // Pick type Logan
      else if (prediction.logan > accuracy) {
        ctx_.filter = 'contrast(400%)';
        ctx_.drawImage(this, 0, 0);
        // Send
        canvas_.toBlob(blob => sendPhoto(blob, CHAT_LOGAN));
        setLine(file.name, 'photo');
      }
    }
    image.src = file.localURL;
  }


  function play() {
    clearTimeout(timeout_);

    if (!train.getWeights()) {
      alert('No training set! Train data!');
      return;
    }

    if (!localStorage.size) {
      alert('No size set! Define size!');
      return;
    }

    if (!$('#play').disabled)
      setLine('Play', 'play_arrow');
    $('#play').disabled = true;
    $('#stop').disabled = false;

    timeout_ = setTimeout(() => {
      resolveLocalFileSystemURL(PATH, fileSystem => {
        fileSystem.createReader().readEntries(filesEntries => {
          filesEntries.forEach(fileEntry => {
            fileEntry.file(file => {
              const { lastModifiedDate, type } = file;

              if (lastModifiedDate > lastModifiedDate_ && type == 'image/jpeg')
                loadIntoCanvas(file);
            });
          });
        }, err => setLine(err, 'report_problem'));
      }, err => setLine(err, 'report_problem'));
      play();
    }, 500);
  }


  function stop() {
    clearTimeout(timeout_);
    if (!$('#stop').disabled)
      setLine('Stop', 'stop');
    $('#play').disabled = false;
    $('#stop').disabled = true;
    lastModifiedDate_ = Date.now();
  }


  // ---------------------------------------------------------------------------
  return {
    load: load
  }
})();
