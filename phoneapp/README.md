# Phoneapp
Android app that listens for new images with relevant bet information in your device, extracts data and resends to other channels.

Notes: 
* chat = channel
* Tokens, IPs, users and ids are examples

# Useful information to run this phone app

## Set up Telegram
1. Allow automatic downloads
2. Allow downloads to the system telegram folder

## Install Android Studio, Gradle and Cordova
1. [Android Studio](https://developer.android.com/studio)
2. [Gradle](https://gradle.org/install/)
3. npm i -g cordova

## Set environment variables
1. Open bashrc file
```sh
nano ~/.bashrc
```
2. Add
```sh
# Android
export ANDROID_SDK_ROOT=~/Android/Sdk
export PATH=${PATH}:~/Android/Sdk/tools:~/Android/Sdk/platform-tools

# Gradle
export PATH=$PATH:/opt/gradle/gradle-6.1.1/bin

```
3. Reload bashrc
```sh
source ~/.bashrc
```

## Run app in Android
Don't forget to connect your phone via usb.
```sh
npm i && cordova platform add android && cordova run android
```

## Run app in a Browser
1. Install local server
```sh
npm i http-server -g
```
2. Set up a server
```sh
cd www && http-server
```