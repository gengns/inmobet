# Enable overrides in devtools
Override Telegram app with minimal changes to track some behaviours.
The changes are private and were used in a private chrome extension. 

1. Page / web.telegram.org / js / app.js
2. Overrides / Select folder for overrides / Allow
3. Change external file
4. Save for Overrides (wait purple dot)
5. Close Chromium (problems: pkill -f -- "chromium --type=renderer")
6. Open devtools in Google.com
7. Open Telegram (devtools have to be opened before page starts loading)

* Note: if any problem occurs disable overrides, load normal mode and enable again