// Go to next step when selector exists
function next_step(selector, success) {
  (function check() {
    if (!document.querySelector(selector)) {
      console.log('Waiting!') 
      setTimeout(() => check(selector), 1000)
    } else if (success) 
      success()
  })();
}

function resendToINMOBET() {
  // Check notifications
  const $badges = 
    document.querySelectorAll('.badge:not(.ng-hide)')
  
  if (!$badges.length)
    return setTimeout(resendToINMOBET, 1000)
  
  // We need to get number of messages before open the channel view
  const num_msgs = Number($badges[0].textContent) 
  
  // Open channel view
  $badges[0].parentElement.parentElement.dispatchEvent(new MouseEvent('mousedown'))

  // Last message
  next_step('.im_dialog_wrap.active:not(.active-remove)', () => {
    Array(num_msgs).fill().forEach((e, i) => {
      document.querySelector(`.im_history_messages_peer:not(.ng-hide) .im_history_message_wrap:nth-last-child(${i + 1}) .im_message_outer_wrap`).click()
    })
    // Forward
    next_step('.im_edit_panel_wrap:not(.ng-hide)', () => {
      document.querySelector('.im_edit_panel_wrap:not(.ng-hide) .im_edit_forward_btn').click()
      // To Inmobet chat
      next_step('.modal-content-animated', () => {
        const $inmobet = [...document.querySelectorAll('.modal-dialog .im_dialog_wrap a')]
        .find(e => e.querySelector('.im_dialog_peer').textContent.trim() == 'INMOBET')
        
        if ($inmobet) {
          // Set into view
          $inmobet.dispatchEvent(new MouseEvent('mousedown'))
          next_step('.composer_rich_textarea:focus', () => {
            // Send
            document.querySelector('button[type="submit"]').dispatchEvent(new MouseEvent('mousedown'))
            setTimeout(resendToINMOBET, 1000)
          })
        } else 
          setTimeout(resendToINMOBET, 1000)
      })
    })
  })
}

setTimeout(resendToINMOBET, 5000)
