# Simulator
Chrome extension that listens for updates in Telegram Web App Channels and resends information (messages and/or images) to main channel (INMOBET) acting as a real user.

Notes: 
* chat = channel
* Tokens, IPs, users and ids are examples

# Useful information to run this chrome extension

## Internal dependecies
[forwarder](https://gitlab.com/gengns/inmobet/-/tree/master/forwarder)


## Add extension to chrome
`chrome://extensions`

1. Click on developer mode
2. Load unpacked
3. Any change need reload the specific chrome extension


## Pack extension
`chrome://extensions`

1. Pack
2. Choose folder
3. Use file.csx

