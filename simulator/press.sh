#!/bin/bash

while true; do
  WID=$(xdotool search --onlyvisible --class chromium|head -1)
  xdotool windowactivate ${WID}
  xdotool key F12
  sleep 5
done
