# Inmobet

Sports bet forecasting system based on analytics from data and tipsters.

## Phoneapp
First of all we created an android app that listens for new images with relevant bet information in your device, extracts data and resends to Telegram channels.

Due to the bad Internet connection and poor hardware of the professional bettors, we decided to move on with a different approach.

## Forwarder and Simulator
Forwarder is a server script that listens for any kind of message in your Telegram channels and resends the information (messages and/or images) after some changes to other channels.

We were using it alone but needed human interaction. Someone who resends relevant Telegram messages to the main channel (the one we were in control of)

Then we added a chrome extension, called Simulator, that listens for updates in Telegram Web App Channels and resends information (messages and/or images) to the main channel.

But bue to time delays, what is crucial when you are betting (it has to be instantaneous), we move on to our last approach.

## Observer
Observer really worked for us, a Chrome extension that listens for updates in Telegram Web App Channels and resends information (messages and/or images) after some changes.

## Gmail
Together with Observer we used the Gmail API to get information from other professional tipsters.

# Resources
* [Brain.js](https://brain.js.org/) 
* [Tesseract.js](https://tesseract.projectnaptha.com/) 
* [Mutation Observers](https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver)
* [Telegram Bots](https://core.telegram.org/bots)


