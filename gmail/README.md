# Gmail
Server script that listens for bet messages in your Gmail and resends the information after some changes to your Telegram channels.

Notes: 
* chat = channel
* Tokens, IPs, users and ids are examples

# Useful information to run this script

## You need a Gmail account
your_account@gmail.com

## Enable gmail api
[Gmail api](https://developers.google.com/gmail/api/quickstart/nodejs#step_1_turn_on_the)

## See chats ids
`https://api.telegram.org/bot<token>/getUpdates`

## Configure ssh in remote host
[ssh in Ubuntu](https://thishosting.rocks/how-to-enable-ssh-on-ubuntu/)
