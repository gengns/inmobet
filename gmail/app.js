// Telegram Chat Ids (set your own names and ids)
const CHAT_TESTING = -1101125292344; // chat for testing
const CHAT_NEROK97 = -1101231402253;
const CHAT_LUKK = -1021104127159;

const { google } = require('googleapis');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

// Telegram token (set your own token) 
const TOKEN = '522161053:AAH0jmAhf9cNSrFbX3PpKLpCxlw6zLtxcTE';
const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(TOKEN, {polling: true});


// Generic text sender
function sendMessage(id, txt) {
  bot.sendMessage(id, txt); // working
  //bot.sendMessage(CHAT_TESTING, txt); // testing
}

/**
 * Lists of messages in the user's account.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
return exports.main = auth => {
  const gmail = google.gmail({version: 'v1', auth});

  // Check every second
  setInterval(() => {
    const hours = new Date().getHours();
    if (hours >= 2 && hours <= 9) return;

    gmail.users.messages.list({
      'userId': 'me', 
      'q': 'in:inbox is:unread category:primary'
    }, (err, res) => {
      if (err) return console.log(err);
      if (!res.data.resultSizeEstimate) return; // empty

      const messageId = res.data.messages[0].id;

      gmail.users.messages.get({
        'userId': 'me',
        'id': messageId
      }, (err, res) => {
        if (err) return console.log(err);
  
        const { headers, body } = res.data.payload;
        const from = headers.find(h => h.name == 'From').value;
        const subject = headers.find(h => h.name == 'Subject').value;
  
        if (from.includes('blogabet')) {
          const html = Buffer.from(body.data, 'base64').toString(); 
          const { document } = (new JSDOM(html)).window;
          const text = document.querySelector("td[colspan]")
            .innerHTML.replace(/<br>/g, '\n');
          
          if (subject.includes('Nerok97')) sendMessage(CHAT_NEROK97, text);
          else if (subject.includes('Lukk')) sendMessage(CHAT_LUKK, text);
          
        } else console.log('else');

        gmail.users.messages.modify({
          'userId': 'me',
          'id': messageId,
          'resource': {
            "addLabelIds": ["STARRED"],
            "removeLabelIds": ["UNREAD"]
          }
        }, (err, res) => {
          if (err) return console.log(err);
        });
      });
    });
  }, 1000);
}


